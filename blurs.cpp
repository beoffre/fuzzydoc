#include "image_ppm.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

auto min(auto a, auto b){
    return(a<b?a:b);
}

auto max(auto a, auto b){
    return(a>b?a:b);
}

float meanVal(OCTET* im, int centerX, int centerY, int kerSize,int nW){
    int mean=0;
    for(int i=(-kerSize+1)/2;i<=(kerSize-1)/2;i++){
        for(int j=(-kerSize+1)/2;j<=(kerSize-1)/2;j++){
            //cout<<i<<", "<<j<<endl;
            mean+=im[(centerX+i)*nW+centerY+j];
        }
    }
    return (float)mean/((float)kerSize*(float)kerSize);
}

float gauss3(OCTET* im, int i, int j, int nW){
    return(
        (1.0/16.0)*(
        4.0*im[i*nW+j]+
        2.0*im[(i+1)*nW+j]+
        2.0*im[(i-1)*nW+j]+
        2.0*im[(i)*nW+j+1]+
        2.0*im[(i)*nW+j-1]+
        1.0*im[(i+1)*nW+j+1]+
        1.0*im[(i-1)*nW+j+1]+
        1.0*im[(i+1)*nW+j-1]+
        1.0*im[(i-1)*nW+j-1]
        ));
}

float gauss5(OCTET* im, int i, int j, int nW){
    return(
        (1.0/273.0)*(
        41.0*im[i*nW+j]+
        26.0*im[(i+1)*nW+j]+
        26.0*im[(i-1)*nW+j]+
        26.0*im[(i)*nW+j+1]+
        26.0*im[(i)*nW+j-1]+
        16.0*im[(i+1)*nW+j+1]+
        16.0*im[(i-1)*nW+j+1]+
        16.0*im[(i+1)*nW+j-1]+
        16.0*im[(i-1)*nW+j-1]+
        7.0*im[(i+2)*nW+j]+
        7.0*im[(i-2)*nW+j]+
        7.0*im[(i)*nW+j+2]+
        7.0*im[(i)*nW+j-2]+
        4.0*im[(i+2)*nW+j+1]+
        4.0*im[(i+2)*nW+j-1]+
        4.0*im[(i-2)*nW+j+1]+
        4.0*im[(i-2)*nW+j-1]+
        4.0*im[(i+1)*nW+j+2]+
        4.0*im[(i-1)*nW+j+2]+
        4.0*im[(i+1)*nW+j-2]+
        4.0*im[(i-1)*nW+j-2]+
        1.0*im[(i+2)*nW+j+2]+
        1.0*im[(i+2)*nW+j-2]+
        1.0*im[(i-2)*nW+j+2]+
        1.0*im[(i-2)*nW+j-2]
        ));
}

int main(int argc, char* argv[]){
    
    
    int nH, nW;
    char cNomImgLue[250];
    
    sscanf(argv[1],"%s",cNomImgLue);
    string inName(cNomImgLue);
    OCTET* imgIn;
    lire_nb_lignes_colonnes_image_pgm(cNomImgLue,&nH,&nW);
    allocation_tableau(imgIn, OCTET, nH*nW);
    lire_image_pgm(cNomImgLue,imgIn,nH*nW);

        
    vector<int> windowSizes=vector<int>();
    for (int i=2; i<argc;i++){
        int nbr = atoi(argv[i]);
        if (!(nbr%2)){
            nbr+=1;
        }
        if (nbr<min(nH,nW)){
            windowSizes.push_back(nbr);
        }        
    }

    OCTET* imgOut;
    allocation_tableau(imgOut,OCTET,nH*nW);
    for (int i=0;i<nH*nW;i++){imgOut[i]=0;}

    for (int ker : windowSizes){


        for (int i=(ker-1)/2;i<=nH-1-(ker-1)/2;i++){
            for (int j=(ker-1)/2; j<=nW-1-(ker-1)/2;j++){
                imgOut[i*nW+j]=floor(meanVal(imgIn,i,j,ker,nW));
                //imgOut[i*nW+j]=floor(gauss5(imgIn,i,j,nW));
            }
        }

        char buffer[50];
        sprintf(buffer,"outBlurs/out%d.pgm",ker);
        ecrire_image_pgm(buffer,imgOut,nH,nW);
    }        
}