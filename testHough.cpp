// #include "image_ppm.h"
// #include <iostream>

// using namespace std;

// auto min(auto a, auto b){
//     return(a<b?a:b);
// }

// auto max(auto a, auto b){
//     return(a>b?a:b);
// }

// int main(){
    
    
//     int nH, nW;

//     OCTET* imgIn;
//     lire_nb_lignes_colonnes_image_pgm("code/lignes.pgm",&nH,&nW);
//     allocation_tableau(imgIn, OCTET, nH*nW);
//     lire_image_pgm("code/lignes.pgm",imgIn,nH*nW);

//     //binarization 
//     for (int i=0; i<nH*nW;i++){
//         if (imgIn[i]!=0){
//             imgIn[i]=255;
//             //cout<<"cc"<<endl;
//         }
//     }

//     int angleAcc=3600,diag=nH*nW;
//     int** tabValues = new int*[angleAcc];//[angleAcc]; = (int**)malloc(angleAcc*sizeof(int*));
//     for (int i=0;i<angleAcc;i++){
//         tabValues[i]=new int[diag];
//         //tabValues[i]=(int*)malloc(diag*sizeof(int));
//     }
//     for (int i=0;i<angleAcc;i++){
//         for (int j=0; j<diag;j++){
//             tabValues[i][j]=0;
//         }
//     }

//     for (int i=0; i<nH;i++){
//         for (int j=0;j<nW;j++){
//             if (imgIn[i*nW+j]!=0){
//                 //cout<<"c"<<endl;
//                 for (int theta=0;theta<angleAcc;theta++){
//                     tabValues[theta][(int)floor(j*cos(M_PI*(float)theta/(float)(angleAcc))+i*sin(M_PI*(float)theta/(float)(angleAcc)))]+=1;
//                 }
//             }
//         }
//     }


//     int minV=tabValues[0][0],maxV=tabValues[0][0];
    
//     for (int i=0;i<angleAcc;i++){
//         for (int j=0;j<diag;j++){
//             minV=min(minV,tabValues[i][j]);
//             maxV=max(maxV,tabValues[i][j]);
//         }        
//     }
//     cout<<"min : "<<minV<<" max : "<<maxV<<endl;

//     OCTET* res;
//     allocation_tableau(res,OCTET, angleAcc*diag);

//     for (int i=0;i<angleAcc;i++){
//         for (int j=0;j<diag;j++){
//             res[i*diag+j]=(((float)tabValues[i][j]-(float)minV)/(float)maxV)*255.0;
//         }        
//     }

//     ecrire_image_pgm("out.pgm",res,angleAcc,diag);



// }

#include "image_ppm.h"
#include <iostream>

using namespace std;

auto min(auto a, auto b){
    return(a<b?a:b);
}

auto max(auto a, auto b){
    return(a>b?a:b);
}

int main(){
    
    
    int nH, nW;
    

    OCTET* imgIn;
    lire_nb_lignes_colonnes_image_pgm("testImages/paraLines.pgm",&nH,&nW);
    allocation_tableau(imgIn, OCTET, nH*nW);
    lire_image_pgm("testImages/paraLines.pgm",imgIn,nH*nW);

    //binarization 
    for (int i=0; i<nH*nW;i++){
        if (imgIn[i]!=0){
            imgIn[i]=255;
            //cout<<"cc"<<endl;
        }
    }

    int diag=trunc(sqrt(nH*nH+nW*nW));
    int angleAcc=(int)((float)diag*16.0/9.0);
    int** tabValues = new int*[diag];//[angleAcc]; = (int**)malloc(angleAcc*sizeof(int*));
    for (int i=0;i<diag;i++){
        tabValues[i]=new int[angleAcc];
        //tabValues[i]=(int*)malloc(diag*sizeof(int));
    }
    for (int i=0;i<diag;i++){
        for (int j=0; j<angleAcc;j++){
            tabValues[i][j]=0;
        }
    }

    for (int i=0; i<nH;i++){
        for (int j=0;j<nW;j++){
            if (imgIn[i*nW+j]!=0){
                
                for (int theta=1;theta<angleAcc-1;theta++){
                    float radTheta = (float)(theta*M_PI)/(float)(angleAcc);
                    int posTab = (int)trunc(i*cos(radTheta)+j*sin(radTheta));
                    if (posTab<0){
                        posTab=diag+posTab-1;
                    }
                    
                    //cout<<posTab<<endl;
                    tabValues[posTab][theta]++;
                }
            }
        }
    }


    int minV=tabValues[0][0],maxV=tabValues[0][0];
    
    for (int i=0;i<diag;i++){
        for (int j=0;j<angleAcc;j++){
            minV=min(minV,tabValues[i][j]);
            maxV=max(maxV,tabValues[i][j]);
        }        
    }
    cout<<"min : "<<minV<<" max : "<<maxV<<endl;

    OCTET* res;
    allocation_tableau(res,OCTET, angleAcc*diag);

    for (int i=0;i<diag;i++){
        for (int j=0;j<angleAcc;j++){
            res[i*angleAcc+j]=(((float)tabValues[i][j]-(float)minV)/(float)maxV)*255.0;
        }        
    }

    ecrire_image_pgm("outPara.pgm",res,diag,angleAcc);



}